import { SymbolValue } from './symbol-value.enum';

export enum MateriaisValue {
  glob = SymbolValue.I,
  prok = SymbolValue.V,
  pish = SymbolValue.X,
  tegj = SymbolValue.L
}
