import { ErrosValue } from './../../shared/enum/erros-value.enum';
import { Component } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { MetaisValue } from './../../shared/enum/metais-value.enum';
/**
 * Componente home da aplicação.
 *
 * @author Guilherme Corrêa
 */
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.component.css']
})

export class HomeComponent {

  calculadora: FormGroup;
  result;
  arrayMateriais = ['glob', 'prok', 'pish', 'tegj'];
  arrayCaracteres = ['I', 'V', 'X', 'L', 'C', 'D', 'M'];

  constructor(private fb: FormBuilder) {
    this.createForm();
  }

  createForm() {
    this.calculadora = this.fb.group({
      pergunta: ['', Validators.required]
    });
  }

  // Inicialização do calculo com o valor de entrada obtido pelo input
  calcularValorTotal() {
    // Obter a pergunta
    const valorAtual = this.calculadora.get(['pergunta']).value;
    const arrayValor = valorAtual.split(' ');

    // Validar se contém frases chaves (Silver, Gold ou Iron)
    const resultFrase = this.validarFrasesChaves(valorAtual.toLowerCase());
    if (resultFrase !== undefined) {
      // Realiza a frase de saída do cálculo
      this.result =  this.formarFraseResultado(arrayValor) + ' is ' + resultFrase + ' Credits';
      return;
    }

    // Válida palavras chaves como (GLOB, PROK, PISH, TEGJ)
    const resultPalavras = this.validarPalavrasChaves(arrayValor);
    if (resultPalavras !== undefined) {
      // Realiza a frase de saída do cálculo
      this.result = this.formarFraseResultado(arrayValor) + ' is ' + this.calcularValores(resultPalavras) + ' Credits';
      return;
    }

    // Válida caracteres chaves como (I, V, X, L, C, D, M)
    const resultCaracteres = this.validarCaracteresValidos(arrayValor);
    if (resultCaracteres != undefined) {
      // Realiza a frase de saída do cálculo
      this.result = this.formarCaractereResultado(arrayValor) + ' is ' + this.calcularValores(arrayValor) + ' Credits';
      return;
    }

    this.result = ErrosValue.Error;
  }

  // Realiza a conversão dos Materiais para Romanos
  converterMateriaisParaRomanos(val) {
    switch (val.toUpperCase()) {
      case  'GLOB':
        return 'I';
      case 'PROK' :
        return 'V';
      case 'PISH' :
        return 'X';
      case 'TEGJ' :
        return 'L';
      default : return;
    }
  }

  // Válida se contém frases chaves (Silver, Gold ou Iron)
  validarFrasesChaves(valorAtual) {
    if (valorAtual.includes('silver')) {
      return this.calcularSilver(valorAtual);
    }
    if (valorAtual.includes('gold')) {
      return this.calcularGold(valorAtual);
    }
    if (valorAtual.includes('iron')) {
      return this.calcularIron(valorAtual);
    }
  }

  // Válida materiais chaves e retorna o cálculo
  validarPalavrasChaves(valorAtual) {
    const arrayValor = [];

    // Filtra os materiais chaves
    valorAtual.forEach((el: { toLowerCase: () => string; }) => {
        const valor = this.arrayMateriais.filter(e => e === el.toLowerCase());
        if (valor.length > 0) {
          arrayValor.push(valor);
        }
    });

    if (arrayValor.length === 0) {
      return undefined;
    }

    // Válida se contém valores repetidos mais de três vezes
    const glob = arrayValor.toString().includes('glob,glob,glob,glob');
    const pish = arrayValor.toString().includes('pish,pish,pish,pish');
    const prok = arrayValor.filter(x => x == 'prok');
    const tegj = arrayValor.filter(x => x == 'tegj');

    if (glob || pish || tegj.length > 1 || prok.length > 1) {
      return;
    }

    let resultado = [];
    // Converte os materiais para Romanos
    arrayValor.forEach(val => {
      const res = this.converterMateriaisParaRomanos(val.toString());
      if (res !== undefined) {
        resultado.push(res);
      }
    });
    return resultado;
  }

  // Válida caracteres validos e retorna o cálculo
  validarCaracteresValidos(valorAtual) {
    const arrayValor = [];

    // Filtra os materiais chaves
    valorAtual.forEach((el: { toUpperCase: () => string; }) => {
      const valor = this.arrayCaracteres.filter(e => e === el.toUpperCase());
      if (valor.length > 0) {
        arrayValor.push(valor);
      }
    });
    if (arrayValor.length === 0) {
      return undefined;
    }

    // Válida se contém valores repetidos mais de três vezes
    const I = arrayValor.toString().includes('I,I,I,I');
    const X = arrayValor.toString().includes('X,X,X,X');
    const C = arrayValor.toString().includes('C,C,C,C');
    const M = arrayValor.toString().includes('M,M,M,M');
    const V = arrayValor.filter(x => x == 'V');
    const L = arrayValor.filter(x => x == 'L');
    const D = arrayValor.filter(x => x == 'D');

    if (I  || X || C || M || V.length > 1 || L.length > 1 || D.length > 1) {
      return;
    }

    let resultado = [];
    // Converte os materiais para Romanos
    arrayValor.forEach(val => {
      const res = this.converterMateriaisParaRomanos(val.toString());
      if (res !== undefined) {
        resultado.push(res);
      }
    });

    return resultado;
  }

  // Calcula os valores Romanos para valores reais
  calcularValores(texto) {
    const textoReplace = texto.toString().replace(/,/g, '');
    let n = 0;
    let nDireita = 0;
    for (let i = textoReplace.length - 1; i >= 0; i--) {
      const valor = this.obterNumero(textoReplace.toUpperCase().charAt(i));
      n += valor * Math.sign(valor + 0.5 - nDireita);
      nDireita = valor;
    }
    return n;
  }

  obterNumero(valor) {
    return Math.floor(Math.pow(10, 'IXCM'.indexOf(valor))) + 5 * Math.floor(Math.pow(10, 'VLD'.indexOf(valor)));
  }

  // Calcula os Produtos Silver
  calcularSilver(valorAtual) {
    const arrayValor = valorAtual.split(' ');
    const valores = this.validarPalavrasChaves(arrayValor);
    if (valores == undefined) {
      return;
    }
    const sumValor = this.calcularValores(valores);
    return sumValor * MetaisValue.silver;

  }

  // Calcula os Produtos Gold
  calcularGold(valorAtual) {
    const arrayValor = valorAtual.split(' ');
    const valores = this.validarPalavrasChaves(arrayValor);
    if (valores == undefined) {
      return;
    }
    const sumValor = this.calcularValores(valores);
    return sumValor * MetaisValue.gold;
  }

  // Calcula os Produtos Iron
  calcularIron(valorAtual) {
    const arrayValor = valorAtual.split(' ');
    const valores = this.validarPalavrasChaves(arrayValor);
    if (valores == undefined) {
      return;
    }
    const sumValor = this.calcularValores(valores);
    return sumValor * MetaisValue.iron;
  }

  // Retorna a frase final para o usuário
  formarFraseResultado(valorAtual) {
    const arrayValor = [];
    valorAtual.forEach((el: { toLowerCase: () => string; }) => {
      const valor = this.arrayMateriais.filter(e => e === el.toLowerCase());
      if (valor.length > 0) {
        arrayValor.push(valor);
      }
    });

    return arrayValor.toString().replace(/,/g, ' ');
  }

  // Retorna a frase final para o usuário
  formarCaractereResultado(valorAtual) {
    const arrayValor = [];
    valorAtual.forEach((el: { toUpperCase: () => string; }) => {
      const valor = this.arrayCaracteres.filter(e => e === el.toUpperCase());
      if (valor.length > 0) {
        arrayValor.push(valor);
      }
    });

    return arrayValor.toString().replace(/,/g, ' ');
  }


// Limpar campos
  resetarValores() {
    this.result = undefined;
  }

}
