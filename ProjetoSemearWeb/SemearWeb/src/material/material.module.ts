import {NgModule} from "@angular/core";

import { CommonModule } from '@angular/common';

import {

  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule, MatTabsModule,

  MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule, MatDatepickerModule, MatProgressBarModule

} from '@angular/material';

@NgModule({

  imports: [

  CommonModule, 

  MatToolbarModule,

  MatButtonModule, 

  MatCardModule,

  MatInputModule,

  MatDialogModule,

  MatTableModule,

  MatMenuModule,

  MatIconModule,

  MatProgressSpinnerModule,

  MatDatepickerModule,

  MatProgressBarModule,

  MatTabsModule

  ],

  exports: [

  CommonModule,

   MatToolbarModule, 

   MatButtonModule, 

   MatCardModule, 

   MatInputModule, 

   MatDialogModule, 

   MatTableModule, 

   MatMenuModule,

   MatIconModule,

   MatProgressSpinnerModule,

   MatDatepickerModule,

   MatProgressBarModule,

   MatTabsModule

   ],

})

export class CustomMaterialModule { }